#ifndef TOOLS_GETOPT_HEADER_FILE
#define TOOLS_GETOPT_HEADER_FILE

#include "types.h"

enum FlagType
{
  SHORT = 0,
  LONG = 1
};

bool isFlag(const char *flag, u8 type);
bool getopt(const char *opt, const char *shortFlag, const char *longFlag);

#endif

