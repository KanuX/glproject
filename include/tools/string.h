#ifndef TOOLS_STRING_HEADER_FILE
#define TOOLS_STRING_HEADER_FILE

#include "types.h"

u16 _strlen(const char *str);
bool _strcmp(const char *str1, const char *str2);
bool flagcmp(const char *str1, const char *str2);

#endif

