#include "tools/string.h"

/* Returns the size of the string. */
u16 _strlen(const char *str)
{
  u16 i = 0;
  bool bKeepLoop = true;

  while(bKeepLoop)
  {
    switch(str[i])
    {
      case '\0': bKeepLoop = false; break;
      default: i++; break;
    }
  }

  return i+1;
}

/* Returns 'true' if strings are equal. Returns 'false' if string are not equal. */
bool _strcmp(const char *str1, const char *str2)
{
  u8 i = 0;
  u16 uStr1len = _strlen(str1),
      uStr2len = _strlen(str2);

  i8 iLenCmp = uStr1len - uStr2len;
  switch(iLenCmp)
  {
    case 0: break;
    default: return false;
  }

  /* Check if each character equals to each other */
  for(i=0; i<uStr1len-1; i++)
  {
    i16 iChar1 = str1[i],
        iChar2 = str2[i];
    const i8 iResult = iChar1 - iChar2;

    switch(iResult)
    {
      case 0: break;
      default: return false;
    }
  }

  return true;
}

/* Returns 'true' if flags are equal. Returns 'false' if string is not a flag or not equal. */
bool flagcmp(const char *str1, const char *str2)
{
  switch(str1[0])
  {
    case '-': break;
    default: return false;
  }
  switch(str2[0])
  {
    case '-': break;
    default: return false;
  }

  bool bFlagCmp = _strcmp(str1, str2);

  return bFlagCmp;
}

